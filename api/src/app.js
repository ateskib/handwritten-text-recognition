const express = require("express");
const session = require('express-session');
const flash = require('connect-flash');

const hbs = require("hbs");
const path = require("path")

const {connectDB} = require("./helpers/db.js")
const {PORT} = require("./configuration/index");
const mainRouter = require("./routers/main")
const uploadRouter = require("./routers/upload")
const recognizeRouter = require("./routers/recognize")

const hbsDataItem = require("./helpers/hbsDataItem")
hbs.registerHelper('hbsDataItem', (item, level) => { return hbsDataItem(item, level); });
hbs.registerHelper('json', (content) => { return JSON.stringify(content); });

const app = express();


app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, '/views'));
hbs.registerPartials(path.join(__dirname + '/views/partials'));

app.use(express.static('public'))
app.use(express.json());
app.use(express.urlencoded());

app.use(session({
    secret: "changeit",
    resave: false,
    saveUninitialized: false
}));
app.use(flash());
app.use(mainRouter);
app.use(uploadRouter);
app.use(recognizeRouter);

function startServer() {
    app.listen(PORT, () => {
        console.log(`Server is listening on ${PORT}`)
    })
}

connectDB()
    .on('error', console.error.bind(console, 'connection error:'))
    .once("open", startServer)