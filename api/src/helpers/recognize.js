const vision = require('@google-cloud/vision');

const recognize = async (fileName) => {

    // Створюємо клієнт
    const client = new vision.ImageAnnotatorClient();
    // Виконуємо запит
    const [result] = await client.documentTextDetection(fileName);
    // Отримуємо результат
    const fullTextAnnotation = result.fullTextAnnotation;
    // Здійснюємо вивід структури даних
    console.log(`Full text: ${fullTextAnnotation.text}`);
    fullTextAnnotation.pages.forEach(page => {
        page.blocks.forEach(block => {
            console.log(`Block confidence: ${block.confidence}`);
            block.paragraphs.forEach(paragraph => {
                console.log(`Paragraph confidence: ${paragraph.confidence}`);
                paragraph.words.forEach(word => {
                    const wordText = word.symbols.map(s => s.text).join('');
                    console.log(`Word text: ${wordText}`);
                    console.log(`Word confidence: ${word.confidence}`);
                    word.symbols.forEach(symbol => {
                        console.log(`Symbol text: ${symbol.text}`);
                        console.log(`Symbol confidence: ${symbol.confidence}`);
                    });
                });
            });
        });
    });
    return fullTextAnnotation;
}

module.exports = recognize;