const hbs = require("hbs");

const levels = {
    pages: {child: 'blocks', current: 'Сторінка'},
    blocks: {child: 'paragraphs', current: 'Блок'},
    paragraphs: {child: 'words', current: 'Параграф'},
    words: {child: 'symbols', current: 'Слово'},
    symbols: {child: false, current: 'Символ'}
}

function hbsDataItem(item, level) {
    var str = "<li class='list-group-item'>";
    str += `<strong>${levels[level].current}</strong>: ${item.confidence.toFixed(3)}`;
    if (levels[level].child) {
        var childLevel = levels[level].child;
        str += "<ul class='list-group'>";
        item[childLevel].forEach((childItem) => {
            str += hbsDataItem(childItem, childLevel);
        });
        str += "</ul>";
    }
    str += "</li>";
    return new hbs.SafeString(str);
}

module.exports = hbsDataItem;