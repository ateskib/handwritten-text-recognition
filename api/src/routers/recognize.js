const recognize = require("../helpers/recognize");
const express = require("express")
const UserImage = require("../models/UserImage");
const path = require("path");
const recognizeRouter = express.Router();
require('dotenv').config()

recognizeRouter.get("/recognize/:id", async function(req, res) {
    const messages = []
    try {
        //Отримуємо зображення з БД
        const image = await UserImage.findOne({_id: req.params.id});
        //Якщо зображення ще не розпізнано
        if (!image.isRecognized) {
            //Отримуємо повний шлях до файлу зображення
            const fileName = path.join(__dirname, '/../../public/uploads/', image.fileName);
            //Розпізнаємо зображення (використовується окремий модуль)
            let fullTextAnnotation = await recognize(fileName);
            //Зберігаємо результат розпізнавання в БД
            image.isRecognized = true;
            image.fullTextAnnotation = fullTextAnnotation;
            await image.save();
            //Генеруємо повідомлення
            messages.push({type: 'success', message: 'Зображення розпізнано'})
        } else {
            messages.push({type: 'warning', message: 'Зображення вже розпізнано'})
        }
        //Робимо перенаправлення на сторінку зображення з відображенням результатів розпізнавання
        res.redirect(`/view/${req.params.id}`);
    } catch (error) {
        messages.push({type: 'danger', message: error.message})
        res.redirect(`/view/${req.params.id}`);
    }
})

module.exports = recognizeRouter