const express = require("express")
const UserImage = require("../models/UserImage");
const mainRouter = express.Router();


mainRouter.get("/view/:id", async (req, res) => {
    try {
        const image = await UserImage.findOne({_id: req.params.id});

        res.render('view', {
            image,
            encodedJson : encodeURIComponent(JSON.stringify(image))
        });
    } catch (error) {
        res.send(error.message)
    }
})

mainRouter.post("/delete", async (req, res) => {
    const messages = []
    console.log(req.body)
    try {
        const image = await UserImage.findOneAndDelete({
            id: req.body.imageId,
            fileName: req.body.imageFileName
        })

        if (!image) {
            throw new Error("Зображення не знайдено")
        }
        messages.push({type: 'success', message: 'Зображення видалено'})
        req.flash('messages', messages)
        res.redirect("/")
    } catch (error) {
        messages.push({type: 'danger', message: error.message})
        req.flash('messages', messages)
        res.status(500).redirect("/");
    }
})

mainRouter.get("/", async (req, res) => {
    try {
        const images = await UserImage.find();

        res.render('main', {
            messages: req.flash('messages'),
            images: images,
            imagesJSON: JSON.stringify(images[0])
        });
    } catch (error) {
        res.send(error.message)
    }
})



module.exports = mainRouter