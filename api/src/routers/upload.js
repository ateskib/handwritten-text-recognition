const upload = require("../helpers/upload");
const express = require("express")
const fs = require("fs")
const { v4: uuidv4 } = require('uuid');

const uploadRouter = express.Router();

const UserImage = require("./../models/UserImage")
const path = require("path");

uploadRouter.get("/upload", (req, res) => {
    res.render("upload.hbs");
})

uploadRouter.post("/upload", upload.single("myImage"), async (req, res) => {
    // req.file can be used to access all file properties
    const messages = []
    try {
        if (!req.file) {
            messages.push({type: 'danger', message: 'Виберіть файл'})
            res.render("/upload", {messages: messages});
        } else {
            const myImage = {
                title: req.body.imageTitle,
                fileName: req.file.filename,
            };
            const userImage = new UserImage(myImage);
            await userImage.save();
            messages.push({type: 'success', message: 'Зображення успішно завантажене'})
            req.flash('messages', messages)
            res.redirect("/")
        }
    } catch (error) {
        messages.push({type: 'danger', message: error.message})
        res.status(500).render("upload", {messages});
    }
});

uploadRouter.get("/create", async (req, res) => {
    const messages = []
    try {
        res.render("create.hbs");
    } catch (error) {
        messages.push({type: 'danger', message: error.message})
        res.status(500).render("create.hbs", {messages});
    }
})

uploadRouter.post("/create", async (req, res) => {
    const messages = []
    try {
        let base64Data = req.body.imageBase64.replace(/^data:image\/png;base64,/, "");

        let fileName = `${uuidv4()}.png`;

        let fullPath = path.join(__dirname, '/../../public/uploads/', fileName);
        fs.writeFileSync(fullPath, base64Data, 'base64');

        const myImage = {
            title: req.body.imageTitle,
            fileName: fileName,
        };
        const userImage = new UserImage(myImage);
        await userImage.save();
        messages.push({type: 'success', message: 'Зображення успішно завантажене'})
        req.flash('messages', messages)
        res.redirect("/")

    } catch (error) {
        messages.push({type: 'danger', message: error.message})
        res.status(500).render("create.hbs", {messages});
    }
})



module.exports = uploadRouter